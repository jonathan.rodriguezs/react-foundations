## Six Lesson: Conditional Rendering

Given the outline of this Message component, how would you implement a ternary expression to display the message inside of a div, or “No Message” if the message prop wasn’t supplied?

> My solution
```javascript
function Message({message}) {
  return (
    <div>
      {
        !message ?
          <div>No Message</div>:
          <div>{message}</div>
      }
    </div>
  )
}
```